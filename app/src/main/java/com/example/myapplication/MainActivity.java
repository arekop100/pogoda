package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.text.Normalizer;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendText(View view) {

        EditText editText = findViewById(R.id.editText);
        String yourText = editText.getText().toString();

        yourText = Normalizer.normalize(yourText, Normalizer.Form.NFD);
        yourText = yourText.replaceAll("[^\\p{ASCII}]", "");

        Intent intent = new Intent(this, Main2Activity.class);

        intent.putExtra("Key", yourText);

        startActivity(intent);


    }
}