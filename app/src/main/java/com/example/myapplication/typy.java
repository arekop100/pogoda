package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

public class typy {
    @SerializedName("main")
    private POST weatherMain;

    public String getTemp(){
        return weatherMain.getTemp().toString() + " °C";
    }
    public String getPressure() {
        return weatherMain.getPressure().toString() + "hPa";
    }

    public String getHumidity() {
        return weatherMain.getHumidity().toString() + " %";
    }

    public String getTemp_min() {
        return weatherMain.getTemp_min().toString() + " °C";
    }

    public String getTemp_max() {
        return weatherMain.getTemp_max().toString() + " °C";
    }
}

