package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface JsonPlaceholderAPI {
    @GET("weather")
    Call<typy> getWeather(
            @Query("q") String city_name,
            @Query("APPID") String appid,
            @Query("units") String units
    );

}
