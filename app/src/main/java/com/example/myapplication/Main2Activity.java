package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.ref.Reference;
import java.text.SimpleDateFormat;
import java.time.LocalTime;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.internal.EverythingIsNonNull;

import static java.util.Calendar.getInstance;

public class Main2Activity extends AppCompatActivity {



    public static final String sharedKey = "Key";
    public static final String sharedKeyCityName = "City";
    private TextView viewTemp,viewPressure,viewHumidity,viewMinTemp,viewMaxTemp,viewCity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        TextView message = findViewById(R.id.display);

        Intent intent = getIntent();
        String miasto = intent.getStringExtra("Key");
        message.setText(miasto);

        viewTemp = findViewById(R.id.viewTemp);
        viewHumidity = findViewById(R.id.viewHumidity);
        viewPressure = findViewById(R.id.viewPressure);
        viewMinTemp = findViewById(R.id.viewTempMin);
        viewMaxTemp = findViewById(R.id.viewTempMax);
        viewCity = findViewById(R.id.display);


        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView dateTime = (TextView) findViewById(R.id.display2);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sf = new SimpleDateFormat("hh:mm:ss");

                                dateTime.setText(sf.format(date));
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        thread.start();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceholderAPI jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);
        final String appid = "749561a315b14523a8f5f1ef95e45864";
        Call<typy> call = jsonPlaceholderAPI.getWeather(miasto + ",pl",appid, "metric");

        call.enqueue(new Callback<typy>() {
            @Override
            public void onResponse(Call<typy> call, Response<typy> response) {
                if(!response.isSuccessful()){
                    viewTemp.setText("Code " + response.code());
                    return;
                }

                typy weather = response.body();


                viewTemp.setText(weather.getTemp());
                viewPressure.setText(weather.getPressure());
                viewHumidity.setText(weather.getHumidity());
                viewMinTemp.setText(weather.getTemp_min());
                viewMaxTemp.setText(weather.getTemp_max());

            }

            @Override
            public void onFailure(Call call, Throwable t) {
                viewTemp.setText(t.getMessage());
            }
        });

    }





}
